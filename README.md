# examen



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/samuel.arteaga.7e6/examen.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/samuel.arteaga.7e6/examen/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

        class Usuario {
            #edad;
            constructor(nombre, apellido, nick, edad) {
                this.nombre = nombre;
                this.apellido = apellido;
                this.nick = nick;
                this.#edad = edad;
            }
        }

        class Publicacion {
            constructor(contenido, fecha, likes) {
                this.contenido = contenido;
                this.fecha = fecha;
                this._likes = likes;
            }

            get likes() {
                return this._likes;
            }

            set likes(value) {
                this._likes = value;
            }
        }

        class Comentario {
            constructor(contenido, fecha, autor) {
                this.contenido = contenido;
                this.fecha = fecha;
                this.autor = autor;
            }
        }

        class UsuarioRedSocial extends Usuario {
            constructor(nombre, apellido, nick, edad) {
                super(nombre, apellido, nick, edad);
                this.publicaciones = [];
                this.comentarios = [];
            }
        }

        class ListaUsuarios {
            constructor() {
                this.usuarios = [];
            }

            insertarUsuario(usuario) {
                if (usuario.publicaciones !== undefined) {
                    this.usuarios.push(usuario);
                }
            }

            obtenerUsuario(nick) {
                return this.usuarios.find(usuario => usuario.nick === nick);
            }

            mostrarUsuarios() {
                this.usuarios.forEach(usuario => {
                    console.log(`Nombre: ${usuario.nombre}, Apellido: ${usuario.apellido}, Nick: ${usuario.nick}`);
                    console.log('Publicaciones:', usuario.publicaciones);
                    console.log('Comentarios:', usuario.comentarios);
                });
            }
        }

        //1
        //Partiendo de un Json inicial, crea los objetos necesarios para almacenar toda esta información.
        const datosRedSocial = JSON.parse(datosRedSocialJson);
        const listaUsuarios = new ListaUsuarios();

        datosRedSocial.usuarios.forEach(usuarioData => {
            const usuario = new UsuarioRedSocial(usuarioData.nombre, usuarioData.apellido, usuarioData.nick, usuarioData.edad);
            listaUsuarios.insertarUsuario(usuario);
        });

        datosRedSocial.publicaciones.forEach(publicacionData => {
            const publicacion = new Publicacion(publicacionData.contenido, publicacionData.fecha, publicacionData.likes);
            const usuario = listaUsuarios.obtenerUsuario(publicacionData.autor);
            if (usuario) {
                usuario.publicaciones.push(publicacion);
            }
        });

        datosRedSocial.comentarios.forEach(comentarioData => {
            const comentario = new Comentario(comentarioData.contenido, comentarioData.fecha, comentarioData.autor);
            const usuario = listaUsuarios.obtenerUsuario(comentarioData.autor);
            if (usuario) {
                usuario.comentarios.push(comentario);
            }
        });

        //2
        //Añade un nuevo usuario TU
        const userTu = new UsuarioRedSocial('samu', 'arteaga', 'samuelito', 20);

        //Añade una nueva publicación a marial y otra a ti
        const publicacionMaria = new Publicacion('Esto es la publicacion de Maria', '2024-02-23', 15);
        const publicacionSamu = new Publicacion('Esto es la publicacion de David', '2024-02-23', 0);
        const usuarioMaria = listaUsuarios.obtenerUsuario('marial');
        usuarioMaria.publicaciones.push(publicacionMaria);
        userTu.publicaciones.push(publicacionSamu);
        listaUsuarios.insertarUsuario(userTu);
        //listaUsuarios.mostrarUsuarios();

        //3
        //Crea un método o función que muestro los datos, publicaciones y comentarios de un usuario concreto (podeis hacerlo directamente con texto HTML). marial
        const mostrarDatosUsuario = (nick) => {
            const usuario = listaUsuarios.obtenerUsuario(nick);
            if (usuario) {
                mostrarContenido(`Nombre: ${usuario.nombre}, Apellido: ${usuario.apellido}, Nick: ${usuario.nick}`);
                mostrarContenido('Publicaciones:');
                usuario.publicaciones.forEach(publicacion => {
                    mostrarContenido(`Contenido: ${publicacion.contenido}, Fecha: ${publicacion.fecha}, Likes: ${publicacion.likes}`);
                });
                mostrarContenido('Comentarios:');
                usuario.comentarios.forEach(comentario => {
                    mostrarContenido(`Contenido: ${comentario.contenido}, Fecha: ${comentario.fecha}, Autor: ${comentario.autor}`);
                });
            }
        }
        mostrarDatosUsuario('marial');
        //4
        //Crea un método o función que muestre toda la información de los usuarios de la red social mostrando sus datos, sus publicaciones y sus comentarios (podeis hacerlo directamente con texto HTML).
        const mostrarTodosDatosUsuarios = () => {
            listaUsuarios.usuarios.forEach(usuario => {
                mostrarContenido(`Nombre: ${usuario.nombre}, Apellido: ${usuario.apellido}, Nick: ${usuario.nick}`);
                mostrarContenido('Publicaciones:');
                usuario.publicaciones.forEach(publicacion => {
                    mostrarContenido(`Contenido: ${publicacion.contenido}, Fecha: ${publicacion.fecha}, Likes: ${publicacion.likes}`);
                });
                mostrarContenido('Comentarios:');
                usuario.comentarios.forEach(comentario => {
                    mostrarContenido(`Contenido: ${comentario.contenido}, Fecha: ${comentario.fecha}, Autor: ${comentario.autor}`);
                });
            });
        }
        mostrarTodosDatosUsuarios();

        function mostrarContenido(texto) {
            const nuevoParrafo = document.createElement('p');
            nuevoParrafo.textContent = texto;

            const h4Resultado = document.querySelector('#resultado');
            h4Resultado.appendChild(nuevoParrafo);
        }